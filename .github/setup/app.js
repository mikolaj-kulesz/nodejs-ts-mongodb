const express = require('express');
const userRouter = require('../routes/users');

function initialize() {
    const app = express();
    app.use(express.json());

    app.use('/users', userRouter);

    app.use("/", (req, res) => {
        res.send('HELLO WORD');
    });

    app.listen(3000);
}

module.exports = { initialize };
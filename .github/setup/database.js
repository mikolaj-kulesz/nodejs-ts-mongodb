require('dotenv').config();
const databaseUrl = process.env.DATABASE_URL
const mongoose = require('mongoose');

function connect () {
    mongoose.connect(databaseUrl, {useNewUrlParser: true});
    const db = mongoose.connection;

    db.on('error', (err) => console.error(err));
    db.once('open', () => console.log(`Connected to MongoDB Database`));
}




module.exports = { connect };
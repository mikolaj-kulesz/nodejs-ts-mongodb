import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    surname: {
        type: String,
        required: true,
    },
    creationDate: {
        type: Date,
        required: true,
        default: Date.now,
    },
    verified: {
        type: Boolean,
        required: true,
        default: false,
    }
})

export default mongoose.model('User', userSchema);


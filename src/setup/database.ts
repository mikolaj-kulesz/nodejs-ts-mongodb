import dotenv from 'dotenv';
import mongoose from 'mongoose';

dotenv.config();
const databaseUrl = process.env.DATABASE_URL || '';

async function connect(): Promise<void> {
  await mongoose.connect(databaseUrl);
  const db = mongoose.connection;

  db.on('error', (err) => console.error(err));
  db.once('open', () => console.log(`Connected to MongoDB Database`));
}

export default {
  connect
}

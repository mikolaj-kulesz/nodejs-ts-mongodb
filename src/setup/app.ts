import express from 'express';
import userRouter from '../routes/users';

function initialize(): void {
  const app = express();
  app.use(express.json());

  app.use('/users', userRouter);

  app.use('/', (_req, res) => {
    console.log('listening!')
    res.send('HELLO WORD');
  });

  app.listen(3000);
}

export default {
  initialize,
};

import express, { NextFunction, Request, Response } from 'express';
import User from '../models/user';

const router = express.Router();

// GET ALL
router.get('/', async (req, res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (err) {
    res.status(500).json({ message: (err as Error).message });
  }
});

// GET ONE
router.get('/:id', getUser, (req, res) => {
  res.json(res.user);
});

// CREATE ONE
router.post('/', async (req, res) => {
  const { name, surname } = req.body;
  const user = new User({
    name,
    surname,
  });
  try {
    const newUser = await user.save();
    res.status(201).json(newUser);
  } catch (err) {
    res.status(400).json({ message: (err as Error).message });
  }
});

// UPDATE ONE
router.patch('/:id', getUser, async (req, res) => {
  const { name, surname, verified } = req.body;
  try {
    await res.user.update({ name, surname, verified });
    const updatedUser = await User.findById(req.params.id);
    res.json(updatedUser);
  } catch (err) {
    res.status(400).json({ message: (err as Error).message });
  }
});

// DELETE ONE
router.delete('/:id', getUser, async (req, res) => {
  try {
    await res?.user.remove();
    res.json({ message: `User removed` });
  } catch (err) {
    res.status(500).json({ message: (err as Error).message });
  }
});

// helpers
async function getUser(req: Request, res: Response, next: NextFunction) {
  let user;
  try {
    user = await User.findById(req?.params?.id);
    if (user == null) {
      return res.status(404).json({ message: 'Cannot find user' });
    }
  } catch (err) {
    return res.status(500).json({ message: (err as Error).message });
  }

  res.user = user;
  next();
}

export default router;
